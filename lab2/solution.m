k = [1, 1, 1, 1, 1]
X = [[1, 0, 0, 0, 1],
     [0, 1, 1, 1, 0],
     [1, 0, 1, 1, 1],
     [0, 0, 1, 1, 1],
     [1, 0, 0, 0, 1]]

rank = 1000
experts_count = rows(X)
     
for h = 1:rank
    weighted_summ = sum(X * k');
    for i = 1:experts_count
        new_k(i) = sum(X(i, :) .* k) / weighted_summ;
    endfor
    
    k = new_k
endfor
      
