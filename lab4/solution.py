from math import *

Xs = [8, 9, 10]
Bs = [4800, 8, 5200, 5400, 5600]

pm = []

def main():
	for X in Xs:
		row = []
		for B in Bs:
			v = min(X * 600, B)
			chees = float(v) / 600 * 50
			a = chees * 150 * 365 -\
			    (50000 + 12 * 1500) * X - 12000 -\
			    B * 4 -\
			    ceil(float(B - v) * 365 / 5000 * 100)
			row.append(a)
			print a, ' ',
		pm.append(row) 
		print ''

def vald():
	print '___vald____'

	for row in pm:
		print min(row)



def savage():
	print '___savage____'

	col_max = map(max, zip(*pm))
	for row in pm:
		print ' '.join((str(col_max[i] - a) for i, a in enumerate(row)))
	for row in pm:
		print max((col_max[i] - a for i, a in enumerate(row)))

def hurwitz():
	alpha = 0.2
	print '___hurwitz____'
	for row in pm:
		print alpha * max(row) + (1 - alpha) * min(row)

def bayes_laplace():
	ps = [3, 4, 5, 4, 3]
	print '___bayes_laplace____'
	nps = [float(p) / sum(ps) for p in ps]
	print nps
	for row in pm:
		print sum((a* p for a, p in zip(row, nps)))



if __name__ == '__main__':
 	main() 
 	vald()
 	savage()
 	hurwitz()
 	bayes_laplace()