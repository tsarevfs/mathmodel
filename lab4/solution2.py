from math import *

Xs = [5, 6, 7, 8, 9, 10]
Bs = [500, 600, 700, 800, 900, 1000]

pm = []

def main():
	for X in Xs:
		row = []
		for B in Bs:
			a = min(X * 100, B) * 0.25 - (max(X * 100, B) - B) * 0.05 
			row.append(a)
			print int(a), '&',
		pm.append(row) 
		print '\\\\ \\hline'

def vald():
	print '___vald____'

	for row in pm:
		print min(row)



def savage():
	print '___savage____'

	col_max = map(max, zip(*pm))
	for row in pm:
		print ' & '.join((str(col_max[i] - a) for i, a in enumerate(row))) + ' & ' + str(max((col_max[i] - a for i, a in enumerate(row)))) + '\\\\ \\hline'
def hurwitz():
	alpha = 0.2
	print '___hurwitz____'
	for row in pm:
		print alpha * max(row) + (1 - alpha) * min(row)

def bayes_laplace():
	ps = [3, 4, 5, 4, 3]
	print '___bayes_laplace____'
	nps = [float(p) / sum(ps) for p in ps]
	print nps
	for row in pm:
		print sum((a* p for a, p in zip(row, nps)))



if __name__ == '__main__':
 	main() 
 	vald()
 	savage()
 	hurwitz()
 	bayes_laplace()