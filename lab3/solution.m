experts_count = 3;
objects_count = 3;

k = ones(objects_count, 1);

R = zeros(3, 3, 3);

R(:, :, 1) = [[0.5, 1, 0],
              [1, 0.5, 1],
              [0, 1, 0.5]];
R(:, :, 2) = [[0.5, 1, 0],
              [0, 0.5, 0],
              [0, 1, 0.5]];
R(:, :, 3) = [[0.5, 1, 1],
              [0, 0.5, 0],
              [1, 1, 0.5]];
     
X = mean(R, 3)

dk = 1

while (all(dk > 0.0001))
    lambda = ones(1, objects_count) * X * k;
    new_k = 1 / lambda * X * k;
    dk = abs(new_k - k);
    k = new_k;
    k'
endwhile

