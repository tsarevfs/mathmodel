from pybrain.tools.shortcuts import buildNetwork
from pybrain.structure import SigmoidLayer
from pybrain.datasets import ClassificationDataSet
from pybrain.supervised.trainers import BackpropTrainer, RPropMinusTrainer

from pybrain.utilities import percentError
from math import *
import random

alldata = ClassificationDataSet(256, class_labels=[0, 1, 2, 3, 4, 5, 6, 7, 8, 9])

fin = open('semeion.data', 'r')
lines = fin.readlines()

for line in lines:
	l = line.strip().split(' ')
	inp = [float(x) for x in l[:256]]
	targ = [int(x) for x in l[256:]].index(1)
	alldata.addSample(inp, (targ,))

tstdata, trndata = alldata.splitWithProportion( 0.25 )
trndata._convertToOneOfMany( )
tstdata._convertToOneOfMany( )

print "Number of training patterns: ", len(trndata)
print "Input and output dimensions: ", trndata.indim, trndata.outdim
print "First sample (input, target, class):"
print trndata['input'][0], trndata['target'][0], trndata['class'][0]

net = buildNetwork( trndata.indim, 32, trndata.outdim, hiddenclass=SigmoidLayer)
trainer = BackpropTrainer(net, dataset=trndata, momentum=0.1, weightdecay=0.01, batchlearning=False)
#trainer = RPropMinusTrainer(net, dataset=trndata, momentum=0.1, verbose=True, weightdecay=0.01)

for i in range(200):
	trnresult = percentError( trainer.testOnClassData(),
	                          trndata['class'] )
	tstresult = percentError( trainer.testOnClassData(
	       dataset=tstdata ), tstdata['class'] )

	print "epoch: %4d" % trainer.totalepochs, \
	      "  train error: %5.2f%%" % trnresult, \
	      "  test error: %5.2f%%" % tstresult

	trainer.trainEpochs(1)