#! /usr/bin/env python
# -*- coding: utf-8 -*-

from functools import partial

import sys
from PyQt4.QtGui import *
from solver import Solver

from add_ingridient_dialog import AddIngridientDialog
from add_recipe_dialog import AddRecipeDialog

import database as db

class MainWindow(QMainWindow):
    def __init__(self):
        QMainWindow.__init__(self)
        self.resize(640, 480)
        self.setWindowTitle("Recipe expert system") 
        self._createMenu()

        self.solver = Solver()

        self._updateQuestion()

    def _updateQuestion(self):
        if self.solver.has_question():
            w = QWidget(self)
            vbox = QVBoxLayout()
            question, answers = self.solver.get_current_question()
            questionLabel = QLabel(question)
            vbox.addWidget(questionLabel)
            spacer = QSpacerItem(20,40,QSizePolicy.Minimum,QSizePolicy.Expanding)
            vbox.addItem(spacer)

            for i, ans in enumerate(answers):
                btn = QPushButton(ans)
                btn.clicked.connect(partial(self._applyAnswer, i))
                vbox.addWidget(btn)

            w.setLayout(vbox)
            self.setCentralWidget(w)
        else:
            w = QWidget(self)
            vbox = QVBoxLayout()
            vbox.addWidget(QLabel('You can try to make folowing recipes:'))
            vbox.addWidget(QLabel('\n'.join(self.solver.get_result_reciepes())))
            spacer = QSpacerItem(20,40,QSizePolicy.Minimum,QSizePolicy.Expanding)
            vbox.addItem(spacer)

            w.setLayout(vbox)
            self.setCentralWidget(w)

    def _applyAnswer(self, idx):
        self.solver.apply_answer(idx)
        self._updateQuestion()


    def _addIngridient(self):
        ingr_classes = db.get_classes()
        dialog = AddIngridientDialog(ingr_classes)
        if dialog.exec_() == QDialog.Accepted:
            name = dialog.nameEdit.text()
            cls = dialog.typeEdit.currentText()
            cost = dialog.costEdit.value()
            energy = dialog.energyEdit.value()
            db.add_ingridient(name, energy, cost, cls)

    def _addRecipe(self):
        ingridients = db.get_ingridients()
        dialog = AddRecipeDialog(ingridients)
        if dialog.exec_() == QDialog.Accepted:
            name = dialog.nameEdit.text()
            instruction = dialog.instruction.toPlainText()
            time = dialog.timeEdit.value()
            ingrs = {}
            for i in range(dialog.table.rowCount()):
                i_name = dialog.table.item(i, 0).text()
                i_count = float(dialog.table.item(i, 1).text())
                ingrs[i_name] = i_count

            db.add_recipe(name, time, instruction, ingrs)

    def _createMenu(self):
        # Create main menu
        mainMenu = self.menuBar()
        #mainMenu.setNativeMenuBar(False)
        fileMenu = mainMenu.addMenu('&File')
        addItemMenu = mainMenu.addMenu('&Add item')
         
        resetAction = QAction('Reset database', self)
        resetAction.triggered.connect(db.reset_database)
        fileMenu.addAction(resetAction)

        exitButton = QAction('Exit', self)
        exitButton.setShortcut('Ctrl+Q')
        exitButton.triggered.connect(self.close)    
        fileMenu.addAction(exitButton)

        # Add ingridient button
        addIngridientButton = QAction('Add ingridient', self)
        addIngridientButton.triggered.connect(self._addIngridient)
        addItemMenu.addAction(addIngridientButton)

        # Add recipe button
        addRecipeButton = QAction('Add recipe', self)
        addRecipeButton.triggered.connect(self._addRecipe)
        addItemMenu.addAction(addRecipeButton)

def main():
    a = QApplication(sys.argv)       

    mw = MainWindow()
    mw.show() 
     
    sys.exit(a.exec_())

if __name__ == '__main__':
    main()