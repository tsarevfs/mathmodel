import database as db
from collections import defaultdict as ddict

class SolverNode:
	def __init__(self, question, answers):
		self.question = question
		self.answers = answers



class Solver:
	def __init__(self):
		self.current = 0
		self.graph = ddict(dict)
		self.nodes = []
		self.subset = None


		vegan_node_id = len(self.nodes)
		self.nodes.append(SolverNode('Are you vegan?',
			[('Yes, I am vegan', lambda subs: db.get_recipes_without_class(['meat', 'milk', 'fish'], subset=subs)),
			 ('No', lambda subs: db.get_recipes_without_class([], subset=subs))]
			))

		class_node_id = len(self.nodes)
		self.nodes.append(SolverNode('Do you what exclude any class of ingridients?',
			[('meat', lambda subs: db.get_recipes_without_class(['meat',], subset=subs)),
			 ('milk', lambda subs: db.get_recipes_without_class(['milk',], subset=subs)),
			 ('fish', lambda subs: db.get_recipes_without_class(['fish',], subset=subs)),
			 ('none', lambda subs: db.get_recipes_without_class([], subset=subs))]
			))

		cost_node_id = len(self.nodes)
		self.nodes.append(SolverNode('How much you ready to spent on portion?',
			[('less than 100', lambda subs: db.get_recipes_by_cost(to=100, subset=subs)),
			 ('from 100 to 300', lambda subs: db.get_recipes_by_cost(fr=100, to=300, subset=subs)),
			 ('more than 300', lambda subs: db.get_recipes_by_cost(fr=300, subset=subs))]
			))


		energy_node_id = len(self.nodes)
		self.nodes.append(SolverNode('How much energy you want meal has?',
			[('less than 100', lambda subs: db.get_recipes_by_energy(to=100, subset=subs)),
			 ('from 100 to 300', lambda subs: db.get_recipes_by_energy(fr=100, to=300, subset=subs)),
			 ('more than 300', lambda subs: db.get_recipes_by_energy(fr=300, subset=subs))]
			))

		time_node_id = len(self.nodes)
		self.nodes.append(SolverNode('How much time do you have?',
			[('less than 10 min', lambda subs: db.get_recipes_by_time(to=10, subset=subs)),
			 ('from 10 to 30 min', lambda subs: db.get_recipes_by_time(fr=10, to=30, subset=subs)),
			 ('more than 30 min', lambda subs: db.get_recipes_by_time(fr=30, subset=subs))]
			))

		self.graph[vegan_node_id][0] = cost_node_id
		self.graph[vegan_node_id][1] = class_node_id
		self.graph[class_node_id][0] = cost_node_id
		self.graph[class_node_id][1] = cost_node_id
		self.graph[class_node_id][2] = cost_node_id
		self.graph[class_node_id][3] = cost_node_id
		self.graph[cost_node_id][0] = energy_node_id
		self.graph[cost_node_id][1] = energy_node_id
		self.graph[cost_node_id][2] = energy_node_id
		self.graph[energy_node_id][0] = time_node_id
		self.graph[energy_node_id][1] = time_node_id
		self.graph[energy_node_id][2] = time_node_id
		self.graph[time_node_id][0] = None
		self.graph[time_node_id][1] = None
		self.graph[time_node_id][2] = None

	def has_question(self):
		return self.current is not None

	def get_current_question(self):
		return (self.nodes[self.current].question, [a[0] for a in self.nodes[self.current].answers])

	def apply_answer(self, answer_id):
		self.subset = self.nodes[self.current].answers[answer_id][1](self.subset)
		self.current = self.graph[self.current][answer_id]
		print self.subset

	def get_result_reciepes(self):
		return self.subset;


def main():
	pass


if __name__ == '__main__':
	main()