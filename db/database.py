from peewee import *
import sys

sqlite_db = SqliteDatabase('recipies.db')

class BaseModel(Model):
    class Meta:
        database = sqlite_db

class IngridentClass(BaseModel):
    name = CharField(unique=True)

class Ingridient(BaseModel):
    name = CharField(unique=True)
    cost = FloatField()
    energy = IntegerField()
    ingridient_class = ForeignKeyField(IngridentClass)

class Recipie(BaseModel):
    name = CharField(unique=True)
    time = IntegerField()
    instruction = CharField()

class Recipie2Ingridients(BaseModel):
    recipe = ForeignKeyField(Recipie)
    ingridient = ForeignKeyField(Ingridient)
    count = FloatField()


def add_class(class_name):
    item = IngridentClass(name=class_name)
    item.save()

def get_classes():
    query = IngridentClass.select()
    return [cls.name for cls in query]

def add_ingridient(ingr_name, ingr_energy, ingr_cost, ingr_class):
    class_item = IngridentClass.get(IngridentClass.name == ingr_class)
    item = Ingridient(name=ingr_name,
                      cost=ingr_cost,
                      energy=ingr_energy,
                      ingridient_class=class_item)
    item.save()

def get_ingridients():
    query = Ingridient.select()
    return [ingr.name for ingr in query]


def add_recipe(recipe_name, recipe_time, recipe_instr, ingridients):
    recipe_item = Recipie(name=recipe_name, time=recipe_time, instruction=recipe_instr)
    recipe_item.save()
    for ingr, cnt in ingridients.items():
        item = Recipie2Ingridients(recipe=recipe_item.id,
                                   ingridient=Ingridient.get(Ingridient.name == ingr).id,
                                   count=cnt)
        item.save()

def get_recipes_by_cost(fr=0, to=sys.maxint, subset=None):
    res = []
    recipes_query =  Recipie.select()
    if subset is not None:
        recipes_query = recipes_query.where(Recipie.name << subset) 
    for recipe in recipes_query:
        query = (Recipie2Ingridients
            .select()
            .join(Ingridient)
            .switch(Recipie2Ingridients)
            .join(Recipie)
            .where(Recipie.id == recipe.id))

        total = sum((q.count * q.ingridient.cost for q in query))
        if fr <= total <= to:
            res.append(recipe.name)

    return res

def get_recipes_by_time(fr=0, to=sys.maxint, subset=None):
    res = []
    recipes_query = None
    if subset is not None:
        recipes_query = Recipie.select().where((Recipie.name << subset) & Recipie.time.between(fr, to)) 
    else:
        recipes_query =  Recipie.select().where(Recipie.time.between(fr, to))
    

    return [r.name for r in recipes_query]

def get_recipes_by_energy(fr=0, to=sys.maxint, subset=None):
    res = []
    recipes_query =  Recipie.select()
    if subset is not None:
        recipes_query = recipes_query.where(Recipie.name << subset) 
    for recipe in recipes_query:
        query = (Recipie2Ingridients
            .select()
            .join(Ingridient)
            .switch(Recipie2Ingridients)
            .join(Recipie)
            .where(Recipie.id == recipe.id))

        total = sum((q.count * q.ingridient.energy for q in query))
        if fr <= total <= to:
            res.append(recipe.name)

    return res

def get_recipes_without_class(ingridient_classes, subset=None):
    res = []
    recipes_query =  Recipie.select()
    if subset is not None:
        recipes_query = recipes_query.where(Recipie.name << subset) 
    for recipe in recipes_query:
        query = (Recipie2Ingridients
            .select()
            .join(Ingridient)
            .switch(Recipie2Ingridients)
            .join(Recipie)
            .where(Recipie.id == recipe.id))

        ok = all((q.ingridient.ingridient_class.name not in ingridient_classes for q in query))
        if ok:
            res.append(recipe.name)

    return res


def get_ingridients_by_recipe(recipe_name):
    query = (Recipie2Ingridients
            .select()
            .join(Ingridient)
            .switch(Recipie2Ingridients)
            .join(Recipie)
            .where(Recipie.name == 'omelette'))


    return [ingr.ingridient.name for ingr in query]


def reset_database():
    try:
        Ingridient.drop_table()
    except:
        pass
    try:
        Recipie.drop_table()
    except:
        pass
    try:
        Recipie2Ingridients.drop_table()
    except:
        pass

    try:
        IngridentClass.drop_table()
    except:
        pass


    Ingridient.create_table()
    Recipie.create_table()
    Recipie2Ingridients.create_table()
    IngridentClass.create_table()

    classes = ['milk', 'meat', 'fish', 'vegetable', 'other']
    for cls in classes:
        add_class(cls)

    print 'database reseted'

def main():
    reset_database()
    ingrs = [('milk', 420, 50, 'milk'),
            ('water', 0, 1, 'other'),
            ('eggs', 80, 5, 'meat'),
            ('salt', 0, 30, 'other'),
            ('pork', 3000, 250, 'meat'),
            ('beef', 2500, 300, 'meat')]

    for a in ingrs:
        add_ingridient(*a)
    

    add_recipe('omelette', 5, 'mix and fry', {'milk' : 0.2, 'eggs' : 3, 'salt' : 0.001})
    add_recipe('steak', 30, 'fry', {'beef' : 0.5, 'salt' : 0.001})

    print get_recipes_by_time(fr=20, subset=['omlette',])


if __name__ == '__main__':
    main()