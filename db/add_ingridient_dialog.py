#! /usr/bin/env python
# -*- coding: utf-8 -*-

import sys
from PyQt4.QtGui import *
from PyQt4.QtCore import Qt

class AddIngridientDialog(QDialog):
    def __init__(self, ingr_classes):
        QDialog.__init__(self)
        self.resize(320, 240)
        self.setWindowTitle("Add ingridient")
        vbox = QVBoxLayout()
        
        grid = QGridLayout()

        nameLabel = QLabel('Name')
        grid.addWidget(nameLabel, 0, 0)
        self.nameEdit = QLineEdit()
        grid.addWidget(self.nameEdit, 0, 1)

        typeLabel = QLabel('Name')
        grid.addWidget(typeLabel, 1, 0)
        self.typeEdit = QComboBox()
        self.typeEdit.addItems(ingr_classes)
        grid.addWidget(self.typeEdit, 1, 0)

        costLabel = QLabel('Cost')
        grid.addWidget(costLabel, 2, 0)
        self.costEdit = QDoubleSpinBox()
        grid.addWidget(self.costEdit, 2, 1)

        energyLabel = QLabel('Energy')
        grid.addWidget(energyLabel, 3, 0)
        self.energyEdit = QSpinBox()
        grid.addWidget(self.energyEdit, 3, 1)

        vbox.addLayout(grid)

        spacer = QSpacerItem(20,40,QSizePolicy.Minimum,QSizePolicy.Expanding)
        vbox.addItem(spacer)

        # OK and Cancel buttons
        buttons = QDialogButtonBox(
            QDialogButtonBox.Ok | QDialogButtonBox.Cancel,
            Qt.Horizontal, self)
        buttons.accepted.connect(self.accept)
        buttons.rejected.connect(self.reject)
        vbox.addWidget(buttons)

        self.setLayout(vbox)
