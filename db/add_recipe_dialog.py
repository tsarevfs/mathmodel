#! /usr/bin/env python
# -*- coding: utf-8 -*-

import sys
from PyQt4.QtGui import *
from PyQt4.QtCore import Qt

class AddRecipeDialog(QDialog):
    def __init__(self, ingridients):
        QDialog.__init__(self)
        self.resize(640, 480)
        self.setWindowTitle("Add recipe")
        vbox = QVBoxLayout()
        
        grid = QGridLayout()

        nameLabel = QLabel('Name')
        grid.addWidget(nameLabel, 0, 0)
        self.nameEdit = QLineEdit()
        grid.addWidget(self.nameEdit, 0, 1)

        timeLabel = QLabel('Time in minutes')
        grid.addWidget(timeLabel, 1, 0)
        self.timeEdit = QSpinBox()
        grid.addWidget(self.timeEdit, 1, 1)

        addIngrLabel = QLabel('Add ingridient')
        grid.addWidget(addIngrLabel, 2, 0)
        self.ingridentEdit = QComboBox()
        self.ingridentEdit.addItems(ingridients)
        grid.addWidget(self.ingridentEdit, 2, 1)
        countLabel = QLabel('Count:')
        grid.addWidget(countLabel, 2, 2)
        self.countEdit = QDoubleSpinBox()
        grid.addWidget(self.countEdit, 2, 3)
        addBtn = QPushButton('Add')
        addBtn.clicked.connect(self.add_clicked)
        grid.addWidget(addBtn, 2, 4)

        vbox.addLayout(grid)


        self.table = QTableWidget()
        self.table.setColumnCount(2)
        self.table.setHorizontalHeaderItem(0, QTableWidgetItem('Ingridient'))
        self.table.setHorizontalHeaderItem(1, QTableWidgetItem('Count'))
        vbox.addWidget(self.table)

        instructionLabel = QLabel('Instruction')
        vbox.addWidget(instructionLabel)
        self.instruction = QPlainTextEdit()
        vbox.addWidget(self.instruction)

        spacer = QSpacerItem(20,40,QSizePolicy.Minimum,QSizePolicy.Expanding)
        vbox.addItem(spacer)

        # OK and Cancel buttons
        buttons = QDialogButtonBox(
            QDialogButtonBox.Ok | QDialogButtonBox.Cancel,
            Qt.Horizontal, self)
        buttons.accepted.connect(self.accept)
        buttons.rejected.connect(self.reject)
        vbox.addWidget(buttons)

        self.setLayout(vbox)

    def add_clicked(self):
        r = self.table.rowCount()
        self.table.insertRow(r)
        self.table.setItem(r, 0, QTableWidgetItem(self.ingridentEdit.currentText()))
        self.table.setItem(r, 1, QTableWidgetItem(str(self.countEdit.value())))

